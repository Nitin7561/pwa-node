var express = require('express');
var app = express();
var path = require('path');
var bodyParser = require('body-parser');
var fs = require('fs');
var https = require('https');


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0



var credentials = {
    key: fs.readFileSync("./certs/star.leantron.dev.key", "utf8"),
    cert: fs.readFileSync("./certs/star.leantron.dev.pem", "utf8"),
};


var PORT_NO_HTTPS = 6979;
// viewed at http://localhost:8081

// app.get('/', function (req, res) {
//     res.sendFile(path.join(__dirname + '/public/index.html'));
// });

app.use('/', express.static(path.join(__dirname, 'public')));

httpsServer = https.createServer(credentials, app);
httpsServer.listen(PORT_NO_HTTPS);
console.log('HTTPS listening on ' + PORT_NO_HTTPS);



